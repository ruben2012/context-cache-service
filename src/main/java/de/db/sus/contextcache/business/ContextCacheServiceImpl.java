/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.business;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.SqlPredicate;

import de.db.sus.contextcache.api.model.InformationContext;
import de.db.sus.contextcache.utils.ContextCacheUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Ruben on 23.06.2017.
 */

@Service
@Slf4j
public class ContextCacheServiceImpl implements ContextCacheService {
    
    private static final String DOT_DELIM = ".";
    private static final String COMMA_DELIMITER = ",";
    
    @Value("${ctx-cache.map.name}")
    private String ctxCacheMapName;
    
    @Autowired
    private HazelcastInstance hzInstance;
    private IMap<String, InformationContext> ctxMap;
    
    @PostConstruct
    private void init() {
	ctxMap = hzInstance.getMap(ctxCacheMapName);
    }
    
    @Override
    public List<InformationContext> getChannelInformationContexts(final String physicalDeviceId, final String channelId) {
	final String partialKey = StringUtils.collectionToDelimitedString(Arrays.asList(physicalDeviceId, channelId), DOT_DELIM);
	
	final Predicate predicate = new SqlPredicate(String.format("__key like %%%s.%%", partialKey));
	final Collection<InformationContext> icsWithPartialKey = ctxMap.values(predicate);
	
	if (icsWithPartialKey.size() > 0) {
	    log.info("found information contexts for [physicalDeviceId={}, channelId={}]", physicalDeviceId,
		    icsWithPartialKey.stream().map(ic -> ic.getMetaData().getId()).collect(Collectors.joining(COMMA_DELIMITER)));
	}
	
	return icsWithPartialKey.parallelStream().collect(Collectors.toList());
    }
    
    @Transactional
    @Override
    public void removeInformationContext(final String physicalDeviceId, final String channelId, final String aboTyp, final String aboVersion,
	    final String aboId) {
	ctxMap.remove(StringUtils.collectionToDelimitedString(Arrays.asList(physicalDeviceId, channelId, aboTyp, aboVersion, aboId), DOT_DELIM));
	log.info("removed information context from map: [physicalDeviceId={}, channelId={}, aboTyp={}, aboVersion={}, aboId={}]", physicalDeviceId,
		channelId, aboTyp, aboVersion, aboId);
    }
    
    @Override
    public InformationContext getInformationContext(final String physicalDeviceId, final String channelId, final String aboTyp,
	    final String aboVersion, final String aboId) {
	final String fullKey = StringUtils.collectionToDelimitedString(Arrays.asList(physicalDeviceId, channelId, aboTyp, aboVersion, aboId),
		DOT_DELIM);
	final InformationContext informationContext = ctxMap.get(fullKey);
	log.info("FULLKEY: {}", fullKey);
	if (informationContext != null) {
	    log.info("found information contexts for [physicalDeviceId={}, channelId={}, aboTyp={}, aboVersion={}, aboId={}]", physicalDeviceId,
		    channelId, aboTyp, aboVersion, aboId);
	}
	return informationContext;
    }
    
    @Transactional
    @Override
    public void addToContextCache(final String physicalDeviceId, final String channelId, final String aboTyp, final String aboVersion,
	    final String aboId, final InformationContext informationContext) {
	final String fullKey = StringUtils.collectionToDelimitedString(Arrays.asList(physicalDeviceId, channelId, aboTyp, aboVersion, aboId),
		DOT_DELIM);
	ctxMap.put(fullKey, informationContext, ContextCacheUtils.secondsToNow(informationContext.getMetaData().getValidUntil()), TimeUnit.SECONDS);
    }
}
