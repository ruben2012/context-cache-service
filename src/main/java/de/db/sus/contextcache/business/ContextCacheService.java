/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */

package de.db.sus.contextcache.business;

import java.util.List;

import de.db.sus.contextcache.api.model.InformationContext;

public interface ContextCacheService {
    void addToContextCache(final String physicalDeviceId, final String channelId, final String aboTyp,
            final String aboVersion, final String aboId, final InformationContext ic);

    List<InformationContext> getChannelInformationContexts(final String physicalDeviceId, final String channelId);

    InformationContext getInformationContext(final String physicalDeviceId, final String channelId, final String aboTyp,
            final String aboVersion, final String aboId);

    void removeInformationContext(final String physicalDeviceId, final String channelId, final String aboTyp,
            final String aboVersion, final String aboId);
}
