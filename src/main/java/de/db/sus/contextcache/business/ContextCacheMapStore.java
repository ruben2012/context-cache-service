/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.MapStore;

import de.db.sus.contextcache.api.model.InformationContext;
import de.db.sus.contextcache.persistence.ContextCacheEntryRepository;
import de.db.sus.contextcache.persistence.model.ContextCacheEntry;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ContextCacheMapStore implements MapStore<String, InformationContext> {

    @Autowired
    private ContextCacheEntryRepository repository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void store(final String key, final InformationContext informationContext) {
	try {
	    repository.save(ContextCacheEntry.builder().key(key).informationContext(objectMapper.writeValueAsString(informationContext)).build());
	} catch (final JsonProcessingException e) {
	    log.error("Could not persist Context Cache Entry for key {} due to Json processing error: {}", key, e.getMessage());
	}
    }

    @Override
    public void storeAll(final Map<String, InformationContext> map) {
	final List<ContextCacheEntry> entries = new ArrayList<>();
	map.forEach((k, v) -> {
	    try {
		entries.add(ContextCacheEntry.builder().key(k).informationContext(objectMapper.writeValueAsString(v)).build());
	    } catch (final JsonProcessingException e) {
		log.error("Could not persist Context Cache Entry for key {} due to Json processing error: {}", k, e.getMessage());
	    }
	});
	repository.save(entries);
    }

    @Override
    public void delete(final String key) {
	repository.delete(key);
    }

    @Override
    public void deleteAll(final Collection<String> keys) {
	repository.delete(keys);
    }

    @Override
    public InformationContext load(final String key) {
	final ContextCacheEntry entry = repository.findOne(key);
	if (entry != null) {
	    try {
		return objectMapper.readValue(entry.getInformationContext(), InformationContext.class);
	    } catch (final IOException e) {
		log.error("Could not read Json for key {} due to error: {}", key, e.getMessage());
	    }
	}
	return null;
    }

    @Override
    public Map<String, InformationContext> loadAll(final Collection<String> keys) {
	return null;
    }

    @Override
    public Iterable<String> loadAllKeys() {
	return null;
    }
}
