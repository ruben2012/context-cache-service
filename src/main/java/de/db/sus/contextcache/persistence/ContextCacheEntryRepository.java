/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.persistence;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.db.sus.contextcache.persistence.model.ContextCacheEntry;

/**
 * Created by Ruben on 25.06.2017.
 */
@Repository
public interface ContextCacheEntryRepository extends JpaRepository<ContextCacheEntry, String> {

    @Query("delete from ContextCacheEntry c where c.key in (:keys)")
    void delete(@Param("keys") Collection<String> keys);

}
