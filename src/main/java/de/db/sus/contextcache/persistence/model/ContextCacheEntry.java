/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Entity
@EqualsAndHashCode(of = "key")
@ToString
@RequiredArgsConstructor(onConstructor = @__({ @JsonCreator }), access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
public class ContextCacheEntry implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ENTRY_KEY")
    @NonNull
    private String key;
    
    @NonNull
    @Column(name = "ENTRY_VALUE", columnDefinition = "TEXT")
    private String informationContext;
    
}
