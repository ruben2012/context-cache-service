/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.config;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.db.sus.contextcache.api.ContextCacheRestApiController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Ruben on 30.06.2017.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("Context-Cache API Spezifikation")
                        .description("Cache zur Pufferung von Informationskontexten").version("1.0")
                        .contact(new Contact("DB Systel - Team BND", null, null)).build())
                .select()
                .apis(RequestHandlerSelectors.basePackage(ContextCacheRestApiController.class.getPackage().getName()))
                .paths(PathSelectors.any()).build().directModelSubstitute(LocalDateTime.class, String.class)
                .enableUrlTemplating(true).host("context-cache-service");
    }
}
