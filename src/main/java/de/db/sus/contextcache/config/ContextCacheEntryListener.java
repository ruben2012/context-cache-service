/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.config;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.MapEvent;

import de.db.sus.contextcache.api.model.InformationContext;
import de.db.sus.contextcache.persistence.ContextCacheEntryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ContextCacheEntryListener implements EntryListener<String, InformationContext> {

  @Autowired
  private ContextCacheEntryRepository repository;

  @Override
  public void entryAdded(EntryEvent<String, InformationContext> entryEvent) {
    // not needed
  }

  @Override
  public void entryEvicted(EntryEvent<String, InformationContext> entryEvent) {
    repository.delete(entryEvent.getKey());
    log.info("Evicted entry with key {}", entryEvent.getKey());
  }

  @Override
  public void entryRemoved(EntryEvent<String, InformationContext> entryEvent) {
    log.info("Removed entry from cache with key {}", entryEvent.getKey());
  }

  @Override
  public void entryUpdated(EntryEvent<String, InformationContext> entryEvent) {
    // not needed
  }

  @Override
  public void mapCleared(MapEvent mapEvent) {
    // not needed
  }

  @Override
  public void mapEvicted(MapEvent mapEvent) {
    // not needed
  }
}
