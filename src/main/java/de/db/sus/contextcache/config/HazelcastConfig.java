/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.EntryListenerConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import de.db.sus.contextcache.business.ContextCacheMapStore;

/**
 * Created by Ruben on 02.06.2017.
 */
@Configuration
public class HazelcastConfig {
    
    @Value("${ctx-cache.hazelcast.cluster.name}")
    private String clusterName;
    
    @Value("${ctx-cache.map.name}")
    private String ctxCacheMapName;
    
    @Value("${ctx-cache.hazelcast.instance.name}")
    private String hzInstanceName;
    
    @Autowired
    private ContextCacheMapStore mapStore;
    
    @Autowired
    private ContextCacheEntryListener entryListener;
    
    @Bean
    public HazelcastInstance hzInstance() {
	final Config config = new Config().setInstanceName(hzInstanceName).setGroupConfig(new GroupConfig(clusterName))
		.setNetworkConfig(new NetworkConfig().setJoin(new JoinConfig().setMulticastConfig(new MulticastConfig().setEnabled(true))))
		.addMapConfig(
			new MapConfig().setName(ctxCacheMapName).addEntryListenerConfig(new EntryListenerConfig().setImplementation(entryListener))
				.setMapStoreConfig(new MapStoreConfig().setImplementation(mapStore).setEnabled(true)));
	return Hazelcast.getOrCreateHazelcastInstance(config);
    }
    
}
