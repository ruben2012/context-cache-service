/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Created by Ruben on 01.07.2017.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ContextCacheUtils {

  /**
   * Gibt die Sekunden zwischen einem zukünftigen Datum und jetzt zurück
   *
   * @param validUntil
   * @return
   */
  public static long secondsToNow(final String validUntil) {
      LocalDateTime parsedValidUntil = LocalDateTime.parse(validUntil, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
      return LocalDateTime.now().until( parsedValidUntil, ChronoUnit.SECONDS);
  }

}
