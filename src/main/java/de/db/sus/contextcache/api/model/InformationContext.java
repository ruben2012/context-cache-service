/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */

package de.db.sus.contextcache.api.model;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import de.db.sus.contextcache.api.serialization.StringBase64Deserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * The information context sent by Context Manager Cache.
 *
 * @author RubenLara
 */
@NoArgsConstructor(onConstructor = @__({ @JsonCreator }))
@RequiredArgsConstructor(staticName = "of")
@ToString
public class InformationContext extends ResourceSupport implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    /**
     * MetaData is a simple wrapper around the actual meta data map to ease
     * access to specific attributes.
     */
    @NoArgsConstructor(onConstructor = @__({ @JsonCreator }), force = true)
    @RequiredArgsConstructor(staticName = "of")
    @Getter
    @ToString
    @EqualsAndHashCode(of = { "id" }, callSuper = false)
    public static class MetaData implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@NonNull
	private String id;
	
	@NonNull
	private Integer prio;
	
	@NonNull
	private String validUntil;
	
	@NonNull
	private String scheduledTime;
	
	@NonNull
	private String outputNode;
	
	@NonNull
	private Long expectedDurationInMilliseconds;
	
	@NonNull
	private String correlationId;
	
	@NonNull
	private String icc;
	
	@NonNull
	private String version;
	
	@NonNull
	private String ic;
	
	@NonNull
	private String abonnementId;
	
	@NonNull
	private String abonnementTyp;
	
	@NonNull
	private String logicalDeviceTarget;
    }
    
    @Getter
    @JsonProperty("meta")
    @NonNull
    private MetaData metaData;
    
    @Getter
    @JsonProperty("data")
    @JsonDeserialize(using = StringBase64Deserializer.class)
    @NonNull
    private String body;
    
}
