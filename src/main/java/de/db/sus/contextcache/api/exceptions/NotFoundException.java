package de.db.sus.contextcache.api.exceptions;

public class NotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public NotFoundException(final String message) {
	super(message);
    }
}
