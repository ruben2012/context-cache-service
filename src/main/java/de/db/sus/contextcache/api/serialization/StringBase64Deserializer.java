/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */

package de.db.sus.contextcache.api.serialization;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class StringBase64Deserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(final JsonParser parser, final DeserializationContext ctx)
            throws IOException, JsonProcessingException {

        final ObjectCodec objCodec = parser.getCodec();
        final JsonNode node = objCodec.readTree(parser);

        final byte[] decodedBase64ByteString = Base64.getDecoder().decode(node.asText());
        return new String(decodedBase64ByteString, StandardCharsets.UTF_8);
    }

}
