/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.api.exceptions;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

import de.db.sus.contextcache.api.ContextCacheRestApiController;
import de.db.sus.contextcache.api.model.JsonError;

@Component
@ControllerAdvice(assignableTypes = ContextCacheRestApiController.class)
public class GlobalExceptionHandler {

    @ExceptionHandler(value = { ConstraintViolationException.class })
    @ResponseBody
    public ResponseEntity<JsonError> handle(final ConstraintViolationException e) {
	final Set<ConstraintViolation<?>> violations = e.getConstraintViolations();

	final JsonError error = JsonError.of(UUID.randomUUID().toString(), "Constraint violation errors", violations.stream()
		.map(v -> JsonError.of(UUID.randomUUID().toString(), v.getMessage(), Lists.newArrayList())).collect(Collectors.toList()));
	return ResponseEntity.badRequest().body(error);
    }
    
    @ExceptionHandler(value = { NotFoundException.class })
    @ResponseBody
    public ResponseEntity<JsonError> handle(final NotFoundException e) {
	final JsonError error = JsonError.of(UUID.randomUUID().toString(), e.getMessage(), Lists.newArrayList());
	return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

}
