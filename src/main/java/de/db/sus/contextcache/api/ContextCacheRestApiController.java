/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 24.06.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */
package de.db.sus.contextcache.api;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import de.db.sus.contextcache.api.exceptions.NotFoundException;
import de.db.sus.contextcache.api.model.InformationContext;
import de.db.sus.contextcache.business.ContextCacheService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Ruben on 23.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(path = "/api")
public class ContextCacheRestApiController {

    @Autowired
    private ContextCacheService contextCacheService;

    /**
     * Schreibt Informationskontext in den Cache
     *
     * @param physicalDeviceId
     * @param channelId
     * @param aboTyp
     * @param aboVersion
     * @param aboId
     * @param informationContext
     * @return Der InformationContext object
     */
    @ApiOperation(value = "Speichert einen Informationskontext in den Cache")
    @PutMapping(path = "/channels/{physicalDeviceId}.{channelId}/ics/{aboTyp}.{aboVersion}.{aboId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> addInformationContext(
	    @ApiParam(value = "ID des physikalischen Gerätes", required = true) @PathVariable final String physicalDeviceId,
	    @ApiParam(value = "ID des channels", required = true) @PathVariable final String channelId,
	    @ApiParam(value = "Typ des Abos", required = true) @PathVariable final String aboTyp,
	    @ApiParam(value = "Version des Abos", required = true) @PathVariable final String aboVersion,
	    @ApiParam(value = "ID des Abos", required = true) @PathVariable final String aboId,
	    @RequestBody @Valid final InformationContext informationContext) {
	contextCacheService.addToContextCache(physicalDeviceId, channelId, aboTyp, aboVersion, aboId, informationContext);
	
	final URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
		.path("/api/channels/{physicalDeviceId}.{channelId}/ics/{aboTyp}.{aboVersion}.{aboId}")
		.buildAndExpand(physicalDeviceId, channelId, aboTyp, aboVersion, aboId).toUri();
	
	return ResponseEntity.noContent().location(location).build();
    }

    /**
     * Löscht Informationskontext aus dem Cache
     *
     * @param physicalDeviceId
     * @param channelId
     * @param aboTyp
     * @param aboVersion
     * @param aboId
     * @return
     */
    @DeleteMapping(path = "/channels/{physicalDeviceId}.{channelId}/ics/{aboTyp}.{aboVersion}.{aboId}")
    @ApiOperation(value = "Löscht einen Informationskontext aus einem Channel im Cache")
    @ApiResponses({ @ApiResponse(code = 404, message = "Informationskontext nicht gefunden") })
    public ResponseEntity<Void> deleteCachedInformationContext(
	    @ApiParam(value = "ID des physikalischen Gerätes", required = true) @PathVariable final String physicalDeviceId,
	    @ApiParam(value = "ID des channels", required = true) @PathVariable final String channelId,
	    @ApiParam(value = "Typ des Abos", required = true) @PathVariable final String aboTyp,
	    @ApiParam(value = "Version des Abos", required = true) @PathVariable final String aboVersion,
	    @ApiParam(value = "ID des Abos", required = true) @PathVariable final String aboId) {
	contextCacheService.removeInformationContext(physicalDeviceId, channelId, aboTyp, aboVersion, aboId);
	
	return ResponseEntity.noContent().build();
    }

    /**
     * Liefert IC der unter dem gegebenen Schlüssel abgelegt ist
     *
     * @param physicalDeviceId
     * @param channelId
     * @param aboTyp
     * @param aboVersion
     * @param aboId
     * @return
     */
    @ApiOperation(value = "Lädt ein konkretes Informationskontext aus dem Cache")
    @GetMapping(path = "/channels/{physicalDeviceId}.{channelId}/ics/{aboTyp}.{aboVersion}.{aboId}")
    @ApiResponses({ @ApiResponse(code = 404, message = "Informationskontext nicht gefunden") })
    public ResponseEntity<InformationContext> getInformationContext(
	    @ApiParam(value = "ID des physikalischen Gerätes", required = true) @PathVariable final String physicalDeviceId,
	    @ApiParam(value = "ID des channels", required = true) @PathVariable final String channelId,
	    @ApiParam(value = "Typ des Abos", required = true) @PathVariable final String aboTyp,
	    @ApiParam(value = "Version des Abos", required = true) @PathVariable final String aboVersion,
	    @ApiParam(value = "ID des Abos", required = true) @PathVariable final String aboId) {
	final InformationContext informationContext = contextCacheService.getInformationContext(physicalDeviceId, channelId, aboTyp, aboVersion,
		aboId);

	if (informationContext == null) {
	    return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	final ResponseEntity<InformationContext> methodLinkBuilder = methodOn(ContextCacheRestApiController.class).getInformationContext(
		physicalDeviceId, channelId, informationContext.getMetaData().getAbonnementTyp(), informationContext.getMetaData().getVersion(),
		informationContext.getMetaData().getAbonnementId());
	final Link self = linkTo(methodLinkBuilder).withSelfRel();
	informationContext.add(self);
	return ResponseEntity.ok(informationContext);
    }

    /**
     * Liefert alle für Teilschlüssel <PhysicalDevice-Id>.<Channel-Id> im Cache
     * hinterlegten Informationskontexte (IC) als Array von
     * Informationskontexten
     *
     * @param physicalDeviceId
     * @param channelId
     * @return
     * @throws NotFoundException
     */
    @GetMapping(path = "/channels/{physicalDeviceId}.{channelId}/ics")
    @ApiOperation(value = "Lädt alle Informationskontexte in einem Channel", response = InformationContext.class, responseContainer = "List")
    public ResponseEntity<List<InformationContext>> getInformationContexts(
	    @ApiParam(value = "ID des physikalischen Gerätes", required = true, example = "123") @PathVariable final String physicalDeviceId,
	    @ApiParam(value = "ID des channels", required = true, example = "456") @PathVariable final String channelId) throws NotFoundException {
	final List<InformationContext> result = contextCacheService.getChannelInformationContexts(physicalDeviceId, channelId);
	
	if (result.isEmpty()) {
	    throw new NotFoundException(
		    String.format("Information context for physicalDeviceId=%s and channelId=%s not found", physicalDeviceId, channelId));
	}

	result.stream().forEach(informationContext -> {
	    final ResponseEntity<InformationContext> methodLinkBuilder = methodOn(ContextCacheRestApiController.class).getInformationContext(
		    physicalDeviceId, channelId, informationContext.getMetaData().getAbonnementTyp(), informationContext.getMetaData().getVersion(),
		    informationContext.getMetaData().getAbonnementId());
	    final Link self = linkTo(methodLinkBuilder).withSelfRel();
	    informationContext.add(self);
	});
	return ResponseEntity.ok(result);
    }
}
