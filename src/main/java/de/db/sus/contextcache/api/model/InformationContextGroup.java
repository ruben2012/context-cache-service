package de.db.sus.contextcache.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 *
 * @author RubenLara
 *
 */
@NoArgsConstructor(onConstructor=@__({@JsonCreator}))
@RequiredArgsConstructor(staticName = "of")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class InformationContextGroup {

    @JsonProperty(value="type", required=true)
    @NonNull
    private String aboType;

    @JsonProperty(value="version", required=true)
    @NonNull
    private String aboVersion;

    @NonNull
    private List<InformationContext> contexts;

}
