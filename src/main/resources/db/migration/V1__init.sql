CREATE SCHEMA IF NOT EXISTS ${ctx_cache_db_schema} AUTHORIZATION ${ctx_cache_db_username};
CREATE TABLE IF NOT EXISTS context_cache_entry (
    entry_key VARCHAR PRIMARY KEY,
    entry_value TEXT NOT NULL
);

