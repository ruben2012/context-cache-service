package de.db.sus.contextcache;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ContextCacheTestUtils {
    
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String readJsonFromFile(final String fileUri) {
	// @formatter:off
	try {
	    return new BufferedReader(new InputStreamReader(new ClassPathResource(fileUri).getInputStream())).lines().collect(Collectors.joining());
	} catch (final IOException e) {
	    log.error("Es konnte nicht aus Classpath-Datei \"{}\" gelesen werden", fileUri);
	    throw new RuntimeException(e);
	}
	// @formatter:on
    }

    public static <T> T toObject(final String jsonString, final Class<T> clazz) {
	try {
	    return mapper.readValue(jsonString, clazz);
	} catch (final IOException e) {
	    log.error("Json konnte nicht zu \"{}\" deserialisiert werden.", clazz);
	    throw new RuntimeException(e);
	}
    }
}
