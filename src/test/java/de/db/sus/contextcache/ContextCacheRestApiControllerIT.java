package de.db.sus.contextcache;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isNull;

import org.assertj.core.util.Strings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.hazelcast.core.HazelcastInstance;

import de.db.sus.contextcache.api.model.InformationContext;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContextCacheRestApiControllerIT {

    private static final String PARTIAL_PATH = "/api/channels/{physicalDeviceId}.{channelId}/ics";
    
    private static final String FULL_PATH = "/api/channels/{physicalDeviceId}.{channelId}/ics/{aboType}.{aboVersion}.{aboId}";
    
    @LocalServerPort
    private int port;

    @Value("${ctx-cache.map.name}")
    private String mapName;
    
    @Autowired
    private HazelcastInstance client;
    
    private static String informationContext_1Json;
    private static String informationContext_2Json;
    private static String informationContext_3Json;
    
    private static String entry1Key = Strings.join("ff8f0f82-5ca0-4a29-99e4-0aa40185f681", "1", "ABFAHRTSTAFEL", "V1", "777").with(".");
    private static String entry2Key = Strings.join("ff8f0f82-5ca0-4a29-99e4-0aa40185f681", "1", "HINWEISTEXT", "V14", "888").with(".");
    private static String entry3Key = Strings.join("b182c962-3ffb-4fa9-bedb-b78ea51affac", "2", "HINWEISTEXT", "V2", "999").with(".");
    
    static {
	informationContext_1Json = ContextCacheTestUtils.readJsonFromFile("fixtures/informationContext_1.json");
	informationContext_2Json = ContextCacheTestUtils.readJsonFromFile("fixtures/informationContext_2.json");
	informationContext_3Json = ContextCacheTestUtils.readJsonFromFile("fixtures/informationContext_3.json");
    }

    @Before
    public void setUp() {
	RestAssured.port = port;
	
	client.getMap(mapName).put(entry1Key, ContextCacheTestUtils.toObject(informationContext_1Json, InformationContext.class));
	client.getMap(mapName).put(entry2Key, ContextCacheTestUtils.toObject(informationContext_2Json, InformationContext.class));
    }
    
    @Test
    public void when_noIcsForChannelExist_then_notFound_returned() throws Exception {
	final String physicalDeviceId = "f94bb2bc-4852-4777-81d8-7e21045360b5";
	final String channelId = "1";
	// @formatter:off
	when()
		.get(PARTIAL_PATH, physicalDeviceId, channelId)
	.then()
		.statusCode(HttpStatus.NOT_FOUND.value())
		.body("message", equalTo(String.format("Information context for physicalDeviceId=%s and channelId=%s not found", physicalDeviceId, channelId)));
	// @formatter:on
    }

    @Test
    public void when_addInformationContextToChannel_then_created_returned() {
	final String[] params = entry3Key.split("\\.");
	// @formatter:off
	given()
		.body(informationContext_3Json)
		.contentType(ContentType.JSON)
		.pathParam("physicalDeviceId", params[0])
		.pathParam("channelId", params[1])
		.pathParam("aboType", params[2])
		.pathParam("aboVersion", params[3])
		.pathParam("aboId", params[4])
	.when()
		.put(FULL_PATH)
	.then()
		.statusCode(HttpStatus.NO_CONTENT.value());
	// @formatter:on
	final InformationContext addedEntry = (InformationContext) client.getMap(mapName).get(entry3Key);
	assertThat(addedEntry, not(isNull()));
    }
    
    @Test
    public void when_icsForChannelExist_then_ok_returned() {
	// @formatter:off
	given()
		.pathParam("physicalDeviceId", "ff8f0f82-5ca0-4a29-99e4-0aa40185f681")
		.pathParam("channelId", "1")
	.when()
		.get(PARTIAL_PATH)
	.then()
		.statusCode(HttpStatus.OK.value())
		.assertThat()
		.body("size()", is(greaterThanOrEqualTo(1)));
	// @formatter:on
    }

    @Test
    public void when_icForChannelExists_then_ok_returned() {
	final String[] params = entry1Key.split("\\.");
	// Get entry1
	// @formatter:off
	final JsonPath groupedContexts =
		given()
        		.pathParam("physicalDeviceId", params[0])
        		.pathParam("channelId", params[1])
        		.pathParam("aboType", params[2])
        		.pathParam("aboVersion", params[3])
        		.pathParam("aboId", params[4])
       		.when()
       			.get(FULL_PATH)
       		.then()
        		.statusCode(HttpStatus.OK.value())
        		.extract()
        		.jsonPath();
	// @formatter:on
	log.debug(groupedContexts.prettify());
    }
    
    @Test
    public void when_icDeleted_then_noContent_returned() {
	// Delete entry2
	final String[] params = entry2Key.split("\\.");
	// @formatter:off
	given()
		.pathParam("physicalDeviceId", params[0])
		.pathParam("channelId", params[1])
		.pathParam("aboType", params[2])
		.pathParam("aboVersion", params[3])
		.pathParam("aboId", params[4])
	.when()
		.delete(FULL_PATH)
	.then()
		.statusCode(HttpStatus.NO_CONTENT.value());
	// @formatter:on
	final InformationContext deletedEntry = (InformationContext) client.getMap(mapName).get(entry2Key);
	assertThat(deletedEntry, is(isNull()));
    }

}
