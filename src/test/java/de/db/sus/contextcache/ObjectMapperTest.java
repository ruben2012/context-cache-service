/*  **************************************************************
 *   Projekt         : blueprint (java)
 *  --------------------------------------------------------------
 *   Autor(en)       : RubenLara
 *   Beginn-Datum    : 06.07.2017
 *  --------------------------------------------------------------
 *   copyright (c) 2015   DB Station&Service AG
 *   Alle Rechte vorbehalten.
 *  **************************************************************
 */

package de.db.sus.contextcache;

import java.util.List;

import org.junit.Test;
import org.springframework.util.Assert;

import de.db.sus.contextcache.api.model.InformationContextGroup;

public class ObjectMapperTest {

    @Test
    public void jsonTextToObject_is_OK() {
	final int expectedValue = 2;
	
	final String json = ContextCacheTestUtils.readJsonFromFile("responses/channel_ics_response.json");
	@SuppressWarnings("unchecked")
	final List<InformationContextGroup> response = ContextCacheTestUtils.toObject(json, List.class);

	Assert.isTrue(response.size() == expectedValue, String.format("Current value %s but expected value was %s", response.size(), expectedValue));
    }

}
